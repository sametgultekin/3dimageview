//
//  HotpointParser.m
//  3DImageView
//
//  Created by Samet Gültekin on 18/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import "HotpointParser.h"
#import <XMLDictionary/XMLDictionary.h>
#import "Hotpoint.h"

@implementation HotpointParser


- (NSArray *)parseHotpointsWithFile:(NSString *)fileName dataPrefix:(NSString *)prefix {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"xml"];
    NSString *hotpointsString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *hotpointsDict = [NSDictionary dictionaryWithXMLString:hotpointsString];
    
    NSInteger hotpointCount = [hotpointsDict[@"count"] integerValue];
    
    NSMutableArray *allHotpoints = [NSMutableArray new];
    
    for (int i = 0; i < hotpointCount; i++) {
        NSString *dataFileName = [NSString stringWithFormat:@"%@_%li", prefix, (long)i];
        NSArray *hotpoints = [self parseDataArrayWithFileName:dataFileName];
        [allHotpoints addObjectsFromArray:hotpoints];
    }
    
    return allHotpoints;
}

- (NSArray*)parseDataArrayWithFileName:(NSString*)fileName {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"xml"];
    NSString *dataString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *dataDict = [NSDictionary dictionaryWithXMLString:dataString]
    ;
    
    NSArray *icons = dataDict[@"icon"];
    
    NSMutableArray *hotpoints = [NSMutableArray new];
    
    for (NSDictionary *hotpointDict in icons) {
        NSInteger hotpointID = [hotpointDict[@"id"] integerValue];
        CGFloat xPosition = [hotpointDict[@"x"] doubleValue];
        CGFloat yPosition = [hotpointDict[@"y"] doubleValue];
        CGPoint position = CGPointMake(xPosition, yPosition);
        Hotpoint *hotpoint = [[Hotpoint alloc] initWithPosition:position hotpointID:hotpointID];
        [hotpoints addObject:hotpoint];
    }
    
    
    return hotpoints;
}

@end