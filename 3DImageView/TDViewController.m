//
//  TDViewController.m
//  3DImageView
//
//  Created by Samet Gültekin on 18/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import "TDViewController.h"
#import "TDImageView.h"
#import "HotpointParser.h"

@interface TDViewController ()
@property (nonatomic, weak) IBOutlet TDImageView *turnableImageView;
@end

@implementation TDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableArray *seqImages = [NSMutableArray new];
    for (int i = 0; i < 36; i++) {
        NSString *imageName = [NSString stringWithFormat:@"sq_%02d", i];
        UIImage *image = [UIImage imageNamed:imageName];
        [seqImages addObject:image];
    }
    self.turnableImageView.sequenceImages = seqImages;
    
    HotpointParser *parser = [[HotpointParser alloc] init];
    NSArray *hotpoints = [parser parseHotpointsWithFile:@"hotpoints" dataPrefix:@"data"];
    self.turnableImageView.hotpoints = hotpoints;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
