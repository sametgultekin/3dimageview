//
//  Weather.h
//  3DImageView
//
//  Created by Samet Gültekin on 18/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject
@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *main;
@property (nonatomic, copy) NSString *weatherDescription;

@property (nonatomic, assign) double temp;
@property (nonatomic, assign) double pressure;
@property (nonatomic, assign) double humidity;
@property (nonatomic, assign) double minTemp;
@property (nonatomic, assign) double maxTemp;

- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
