//
//  HotpointParser.h
//  3DImageView
//
//  Created by Samet Gültekin on 18/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotpointParser : NSObject
- (NSArray*)parseHotpointsWithFile:(NSString*)fileName dataPrefix:(NSString*)prefix;

@end
