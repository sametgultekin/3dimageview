//
//  main.m
//  3DImageView
//
//  Created by Samet Gültekin on 17/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
