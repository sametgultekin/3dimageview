//
//  Hotpoint.m
//  3DImageView
//
//  Created by Samet Gültekin on 17/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import "Hotpoint.h"

@implementation Hotpoint

- (instancetype)initWithPosition:(CGPoint)position hotpointID:(NSInteger)hotpointID {
    
    self = [super init];
    
    if (self) {
        _position   = position;
        _hotpointID = hotpointID;
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"HotpointID:%li, Position:%@", self.hotpointID, NSStringFromCGPoint(self.position)];
}

@end
