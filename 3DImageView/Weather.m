//
//  Weather.m
//  3DImageView
//
//  Created by Samet Gültekin on 18/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import "Weather.h"

@implementation Weather


- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    
    self = [super init];
    
    if (self) {
        
        self.name = dictionary[@"name"];
        
        NSDictionary *weatherDict = [dictionary[@"weather"] firstObject];
        self.main                   = weatherDict[@"main"];
        self.weatherDescription     = weatherDict[@"description"];
        
        NSDictionary *mainDict = dictionary[@"main"];
        self.temp       =   [mainDict[@"temp"] doubleValue];
        self.pressure   =   [mainDict[@"pressure"] doubleValue];
        self.humidity   =   [mainDict[@"humidity"] doubleValue];
        self.minTemp    =   [mainDict[@"temp_min"] doubleValue];
        self.maxTemp    =   [mainDict[@"temp_max"] doubleValue];
        
    }
    
    return self;
    
}

@end
