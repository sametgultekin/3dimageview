//
//  WeatherDetailViewController.h
//  3DImageView
//
//  Created by Samet Gültekin on 18/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Weather.h"

@interface WeatherDetailViewController : UIViewController
@property (nonatomic, strong) Weather *weather;
@end
