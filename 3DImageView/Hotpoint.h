//
//  Hotpoint.h
//  3DImageView
//
//  Created by Samet Gültekin on 17/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Hotpoint : NSObject
@property (nonatomic, readonly) CGPoint position;
@property (nonatomic, readonly) NSInteger hotpointID;

- (instancetype)initWithPosition:(CGPoint)position hotpointID:(NSInteger)hotpointID;

@end
