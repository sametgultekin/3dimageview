//
//  CountryTableViewController.m
//  3DImageView
//
//  Created by Samet Gültekin on 18/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import "CountryTableViewController.h"
#import "Weather.h"
#import <AFNetworking/AFNetworking.h>
#import "WeatherDetailViewController.h"

@interface CountryTableViewController ()

@property (nonatomic, strong) NSArray *weatherInfos;
@end

@implementation CountryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    self.title = @"Countries";
    
    [self refresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)refresh {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:@"http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric"
      parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.refreshControl endRefreshing];
        
        if (responseObject) {
            NSArray *infos = [(NSDictionary*)responseObject objectForKey:@"list"];
            NSMutableArray *weatherInfos = [NSMutableArray new];
            for (NSDictionary *infoDict in infos) {
                Weather *weatherInfo = [[Weather alloc] initWithDictionary:infoDict];
                [weatherInfos addObject:weatherInfo];
            }
            self.weatherInfos = weatherInfos;
            [self.tableView reloadData];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self presentErrorAlertView];
        [self.refreshControl endRefreshing];
        
        
    }];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.weatherInfos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    Weather *weather = self.weatherInfos[indexPath.row];
    
    cell.textLabel.text = weather.name;
    
    
    return cell;
}

- (void)presentErrorAlertView {
    
    UIAlertController * alertController=   [UIAlertController
                                  alertControllerWithTitle:@"Error"
                                  message:@"Connection Error"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction
                         actionWithTitle:@"Retry"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self refresh];
                             [alertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancelAction = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender {
    
    if ([segue.identifier isEqualToString:@"WeatherDetailSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Weather *weather = self.weatherInfos[indexPath.row];
        
        WeatherDetailViewController *detail = (WeatherDetailViewController*)[segue destinationViewController];
        detail.weather = weather;
        
        
    }
}

@end
