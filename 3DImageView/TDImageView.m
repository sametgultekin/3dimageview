//
//  TDImageView.m
//  3DImageView
//
//  Created by Samet Gültekin on 17/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import "TDImageView.h"
#import "Hotpoint.h"

@interface TDImageView()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, strong) CAShapeLayer *hotpointLayer;

@property (nonatomic, strong) NSArray *activeHotpoints;
@property (nonatomic, assign) NSInteger currentSequenceIndex;

@end

@implementation TDImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    

    [super drawRect:rect];
}
*/


- (nonnull instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    return self;
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setSequenceImages:(NSArray *)sequenceImages {
    
    _sequenceImages = sequenceImages;
    self.currentSequenceIndex = 0;
}

- (void)setHotpoints:(NSArray *)hotpoints {

    _hotpoints = [self filterNonZeroHotpoints:hotpoints];
}

- (void)setCurrentSequenceIndex:(NSInteger)currentSequenceIndex {
    
    if (currentSequenceIndex < 0 || currentSequenceIndex >= self.sequenceImages.count) {
        return;
    }
    
    _currentSequenceIndex = currentSequenceIndex;
    
    UIImage *currentImage = self.sequenceImages[currentSequenceIndex];
    self.imageView.image  = currentImage;
    
    [self updateHotpointLayer];
}

- (void)skipSequence:(NSInteger)skipCount {
    
    NSInteger imageCount = self.sequenceImages.count;
    
    if (imageCount == 0) {
        return;
    }
    
    if (self.reverseDirection) {
        skipCount = skipCount * -1;
    }
    
    NSInteger destinationIndex = 0;
    if (self.currentSequenceIndex + skipCount > imageCount -1) {
        destinationIndex = (self.currentSequenceIndex + skipCount) - imageCount;
    }
    else if (self.currentSequenceIndex + skipCount < 0) {
        destinationIndex = imageCount - self.currentSequenceIndex + skipCount;
    }
    else {
        destinationIndex = self.currentSequenceIndex + skipCount;
    }
    
    [self setCurrentSequenceIndex:destinationIndex];
    
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    [_imageView setFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    [self updateHotpointLayer];
    
}

- (void)updateHotpointLayer {
    
    
    [self.hotpointLayer removeFromSuperlayer];
    
    NSArray *filteredArray = [self filterActiveHotpoints:self.hotpoints sequenceIndex:self.currentSequenceIndex];
    self.activeHotpoints = filteredArray;
    
    CGMutablePathRef pointPaths = CGPathCreateMutable();
    
    for (Hotpoint *hotpoint in self.activeHotpoints) {
        CGPoint translatedPoint = [self translatePoint:hotpoint.position];
        UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(translatedPoint.x, translatedPoint.y, 10, 10)];
        CGPathAddPath(pointPaths, NULL, bezierPath.CGPath);
    }
    
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.path = pointPaths;
    layer.position = CGPointZero;
    layer.fillColor = [UIColor redColor].CGColor;
    [self.layer addSublayer:layer];
    self.hotpointLayer = layer;
}

#pragma mark - Setup

- (void)setup {
    
    if (self.imageView) {
        [self.imageView removeFromSuperview];
        self.imageView = nil;
    }
    
    if (self.panGestureRecognizer) {
        [self removeGestureRecognizer:self.panGestureRecognizer];
        self.panGestureRecognizer = nil;
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    imageView.clipsToBounds = YES;
    [self addSubview:imageView];
    self.imageView = imageView;
    
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
    [self addGestureRecognizer:recognizer];
    self.panGestureRecognizer = recognizer;
    
    self.reverseDirection = YES;
    self.opaque = NO;
}



#pragma mark - Gesture Recognizer

- (void)panGestureRecognized:(UIPanGestureRecognizer*)recognizer {
    
    CGPoint translatedPoint = [recognizer translationInView:self.superview];
    CGFloat xPosition = translatedPoint.x;
    
    if (fabs(xPosition) > 10.0) {
        NSInteger changeCount = floor(xPosition / 10);
        [self skipSequence:changeCount];
        [recognizer setTranslation:CGPointZero inView:self.superview];
    }
}


#pragma mark - Translate point Using Size

- (CGPoint)translatePoint:(CGPoint)point {
    
    CGSize viewSize = self.frame.size;
    CGSize imageSize = self.imageView.image.size;
    
    CGPoint translatedPoint = CGPointZero;
    translatedPoint.x = point.x * (viewSize.width / imageSize.width);
    translatedPoint.y = point.y * (viewSize.height / imageSize.height);
    return translatedPoint;
}

#pragma mark - Remove zero Hotpoints

- (NSArray*)filterNonZeroHotpoints:(NSArray*)hotpoints {
    
    NSMutableArray *nonZeroHotPoints = [NSMutableArray new];
    
    for (Hotpoint *hotpoint in hotpoints) {
        if (!CGPointEqualToPoint(hotpoint.position, CGPointZero)) {
            [nonZeroHotPoints addObject:hotpoint];
        }
    }
    return nonZeroHotPoints;
}

- (NSArray*)filterActiveHotpoints:(NSArray*)hotpoints sequenceIndex:(NSInteger)sequenceIndex {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hotpointID = %i", sequenceIndex];
    NSArray *filteredArray = [hotpoints filteredArrayUsingPredicate:predicate];
    
    return filteredArray;
}


@end
