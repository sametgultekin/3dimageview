//
//  WeatherDetailViewController.m
//  3DImageView
//
//  Created by Samet Gültekin on 18/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import "WeatherDetailViewController.h"

@interface WeatherDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *minTempLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxTempLabel;

@end

@implementation WeatherDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self updateLabels];
    
    self.title = self.weather.name;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setWeather:(Weather *)weather {
    _weather = weather;
    [self updateLabels];
}

- (void)updateLabels {
    self.nameLabel.text         = self.weather.name;
    self.mainLabel.text         = self.weather.main;
    self.descriptionLabel.text  = self.weather.weatherDescription;
    self.tempLabel.text         = [NSString stringWithFormat:@"Temp: %.2f", self.weather.temp];
    self.pressureLabel.text     = [NSString stringWithFormat:@"Pressure: %.2f", self.weather.pressure];
    self.humidityLabel.text     = [NSString stringWithFormat:@"Humidity: %.2f", self.weather.humidity];
    self.minTempLabel.text      = [NSString stringWithFormat:@"Min Temp: %.2f", self.weather.minTemp];
    self.maxTempLabel.text      = [NSString stringWithFormat:@"Max Temp:%.2f", self.weather.maxTemp];
}

@end
