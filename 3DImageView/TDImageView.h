//
//  TDImageView.h
//  3DImageView
//
//  Created by Samet Gültekin on 17/08/15.
//  Copyright © 2015 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDImageView : UIView

@property (nonatomic, strong) NSArray *sequenceImages;

@property (nonatomic, strong) NSArray *hotpoints;

// Reverses Pan direction - Default is YES
@property (nonatomic) BOOL reverseDirection;

@end
